package com.example.loginactivity;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class DestinationItemTest {
    DestinationItem dest;
    private String mDestinationName = "destination";
    private int mDestinationImage = 113;
    private String mDestinationDesc = "description";

    @Before
    public void setup() {
        dest = new DestinationItem(mDestinationName, mDestinationImage, mDestinationDesc);
    }

    @Test
    public void getdestinationName() {
        assertEquals("Not correct", mDestinationName, dest.getdestinationName());
    }

    @Test
    public void getdestinationImage() {
        assertEquals("Not correct", mDestinationImage, dest.getdestinationImage());
    }

    @Test
    public void setmDestinationDesc() {
        dest.setmDestinationDesc(mDestinationDesc);
        assertEquals("Not correct", mDestinationDesc, dest.getmDestinationDesc());
    }
}