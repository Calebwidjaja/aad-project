package com.example.loginactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class mainMenu extends AppCompatActivity {

    private TextView textView;
    private Button map;
    Button destination1;
    Button profile;
    Button signOut;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainmenu);

        textView = (TextView) findViewById(R.id.userHeader);
        Typeface customheader = Typeface.createFromAsset(getAssets(), "fonts/Pokemon Hollow.ttf");
        textView.setTypeface(customheader);

        // go to map page
        map = (Button) findViewById(R.id.mapbutton);
        map.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent1 = new Intent(mainMenu.this, MapsActivity.class);
                startActivity(intent1);
            }
        });

        // go to destination page
        destination1 = (Button) findViewById(R.id.destinationButton);
        destination1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainMenu.this, destination.class);
                startActivity(intent);
            }
        });

        // go to intro page
        profile = (Button) findViewById(R.id.profilebutton);
        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mainMenu.this, com.example.loginactivity.profile.class);
                startActivity(intent);
            }
        });

        signOut = (Button) findViewById(R.id.signOut);
        signOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(mainMenu.this, MainActivity.class);
                startActivity(i);
            }
        });
    }
}
