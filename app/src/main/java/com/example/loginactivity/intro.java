package com.example.loginactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class intro extends AppCompatActivity {

    private TextView title;
    private Button button1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_intro);
        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) { // Landscape mode
        } else {
        }

        title = (TextView) findViewById(R.id.header);
        Typeface customFont = Typeface.createFromAsset(getAssets(), "fonts/Pokemon Hollow.ttf"); // custom font
        title.setTypeface(customFont);

        button1 = (Button) findViewById(R.id.enterButton);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(intro.this, mainMenu.class);
                startActivity(intent);
            }
        });

    }
}
