package com.example.loginactivity;

public class DestinationItem {
    private String mDestinationName;
    private int mDestinationImage;
    private String mDestinationDesc;


    //Constructor to pass the value in for the country
    public DestinationItem(String destinationName, int destinationImage, String destinationDesc) {
        mDestinationName = destinationName;
        mDestinationImage = destinationImage;
        mDestinationDesc = destinationDesc;

    }

    // to
    public DestinationItem() {
    }

    public String getdestinationName() {
        return mDestinationName;
    }

    public int getdestinationImage() {
        return mDestinationImage;
    }

    public void setmDestinationDesc(String mDestinationDesc) {
        this.mDestinationDesc = mDestinationDesc;
    }

    public String getmDestinationDesc() {
        return mDestinationDesc;
    }
}
